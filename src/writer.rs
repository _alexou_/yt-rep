use crate::yt_scraper::VideoInfo;
use std::error::Error;
use std::fmt::format;
use std::fs::write;
use std::path::PathBuf;
pub fn format_as_json(infos: Vec<VideoInfo>) -> Result<String, Box<dyn Error>> {
    let mut formatted_string = r#"{
        "videos":["#.to_string();
    for i in infos{
        let video_instance =format!(r#"
            {{
            "title": "{}",
            "channel": "{}",
            "link_to_video": "{}"
        }},"#,i.title, "channel", i.link_to_video);
        formatted_string += video_instance.as_str();
    };
    formatted_string+= "]}";
    Ok(formatted_string)
}


pub fn format_as_html(infos: Vec<VideoInfo>) -> Result<(), Box<dyn Error>> {
    todo!()
}

pub fn format_as_text(infos: Vec<VideoInfo>) -> Result<String, Box<dyn Error>> {
    let mut formatted_string = String::new();
    for i in infos {
        formatted_string += format!("{}\n", i.link_to_video).as_str();
    }
    Ok(formatted_string)
}

pub fn write_file(content: String, file: PathBuf)-> Result<(), Box<dyn Error>> {
    write(file, content)?;
    Ok(())
}
