use headless_chrome::{Browser, LaunchOptionsBuilder};
use scraper::{Html, Selector};
use std::error::Error;
use std::time::Duration;

pub struct VideoInfo {
    pub title: String,
    pub channel: String,
    pub link_to_video: String,
}

fn format_search_url(query: String) -> Result<String, Box<dyn Error>> {
    Ok(format!(
        "https://www.youtube.com/results?search_query={}",
        query
    ))
}

// makes the actual request to youtube
pub fn scrape_with_browser(query: String) -> Result<String, Box<dyn Error>> {
    // let browser = Browser::default()?;
    let browser = Browser::new(
        LaunchOptionsBuilder::default()
            .headless(true)
            .build()
            .expect("Failed to create launch options"),
    )
    .unwrap();
    println!("browser created");

    let tab = browser.new_tab()?;
    tab.navigate_to(&format_search_url(query)?)?;
    println!("waiting for elements to load...");
    // tab.wait_until_navigated()?;
    tab.wait_for_element("div#contents")?; //waits until the js DOM content has loaded the important elements
    let html = tab.get_content()?;
    // let pg_down = tab.wait_for_element("body")?;

    // let body_selector = Selector::parse("body").unwrap();
    // let document = Html::parse_document(&html);
    // println!("\n\n\n\n\n{}", document.html());
    Ok(html)
}

pub fn parse_results(raw_html: String) -> Result<Vec<VideoInfo>, Box<dyn Error>> {
    // selectors used to parse the result
    let video_list_selector =
        Selector::parse("ytd-item-section-renderer.ytd-section-list-renderer")?;
    let video_link_selector =
        Selector::parse("a.yt-simple-endpoint, a.style-scope, a.ytd-video-renderer")?;
    let video_element_selector = Selector::parse("ytd-video-renderer.ytd-item-section-renderer")?;
    let video_title_selector = Selector::parse("yt-formatted-string.ytd-video-renderer")?;
    let channel_name_selector =
        Selector::parse("a.yt-simple-endpoint, a.style-scope, a.yt-formatted-string")?;

    let document = Html::parse_document(&raw_html);
    let video_list = document.select(&video_list_selector).next().unwrap();

    let mut video_results: Vec<VideoInfo> = Vec::new();

    for video in video_list.select(&video_element_selector) {
        let title = video
            .select(&video_title_selector)
            .next()
            .unwrap()
            .value()
            .attr("aria-label")
            .unwrap()
            .to_string();

        let channel: String = video
            .select(&channel_name_selector)
            .next()
            .unwrap()
            .text()
            .collect();

        let mut link_to_video = video
            .select(&video_link_selector)
            .next()
            .unwrap()
            .value()
            .attr("href")
            .unwrap()
            .to_string();
        link_to_video = format!("https://youtube.com{}", link_to_video);


        let parsed_video = VideoInfo {
            title: title,
            channel: channel,
            link_to_video: link_to_video,
        };
        video_results.push(parsed_video)
    }
    Ok(video_results)
}
