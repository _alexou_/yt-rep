// use crate::writer::output_html;
use clap::Parser;
// use scraper::{Html, Selector};
// use serde_json;
use std::path::PathBuf;

mod writer;
mod yt_scraper;

fn main() {
    let args = Args::parse();

    let raw_html = yt_scraper::scrape_with_browser(args.url.join(" ")).unwrap();
    let parsed_results = yt_scraper::parse_results(raw_html).unwrap();
    let mut formatted_output = String::new();

    // choses the correct format to display
    match args.format.as_str() {
        "html" => println!("html"),
        "json" => formatted_output = writer::format_as_json(parsed_results).unwrap(),
        "text"|"txt" => formatted_output = writer::format_as_text(parsed_results).unwrap(),
        "struct" => println!("struct"),
        _ => panic!("The format is not valid, {}", args.format),
    }
    
    // prints the file if the option is used
    match args.output{
        None => println!("{}", formatted_output),
        Some(output_file)=> writer::write_file(formatted_output, output_file).expect("cannot write the file")
    }

}

/// A youtube scraping program that can filter results
#[derive(Parser, Debug)]
#[command(author = "_alexou_", version, about, long_about = None)]
struct Args {
    /// Search query
    #[clap()]
    url: Vec<String>,

    /// Only return results from this channel
    #[arg(short, long, value_name = "CHANNEL")]
    channel: Option<String>,

    /// Only return results that contain specific words in their title
    #[arg(short, long, value_name = "TITLE")]
    title: Option<String>,

    /// Only return results that contain a certain description
    #[arg(short, long, value_name = "DESCRIPTION")]
    description: Option<String>,

    /// Output the result as a file
    #[arg(short, long, value_name = "FILE")]
    output: Option<PathBuf>,

    /// Format the output
    #[arg(short, long, value_name = "HTML, JSON, TEXT, STRUCT")]
    format: String,
}
